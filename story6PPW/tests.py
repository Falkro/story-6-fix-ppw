from re import search

from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
from .models import Status


# Create your tests here.



class ProfUnitTest(TestCase):
	def test_url_home_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_url_addstatus_exist(self):
		response = Client().get('/addstatus')
		self.assertEqual(response.status_code, 200)
		
	def test_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, views.index)

	def test_using_addstatus_func(self):
		found = resolve('/addstatus')
		self.assertEqual(found.func, views.addstatus)

	def test_landing_page(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>Status Anda</title>', html_response)
		self.assertIn('<h1>Hello, Tuliskan statusmu disini!</h1>', html_response)

	def test_landing_html(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'index.html')

	def test_addstatus_html(self):
		response = Client().get('/addstatus')
		self.assertTemplateUsed(response, 'addstatus.html')

	def test_model_can_create_new_activity(self):
		new_activity = Status.objects.create(status="Ini Status Test")
		all_activities = Status.objects.all().count()
		self.assertEqual(all_activities, 1)

	def test_form_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<form id="form" method="POST" action="/addstatus">', html_response)

	def test_submit_button_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<button type="submit">', html_response)

	def test_auto_redirect(self):
		request = HttpRequest()
		response = views.addstatus(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<meta http-equiv="refresh"', html_response)
		
	def test_url_profile_exist(self):
		response = Client().get('/profil')
		self.assertEqual(response.status_code, 200)

	def test_profile_html(self):
		response = Client().get('/profil')
		self.assertTemplateUsed(response, 'profil.html')

	def test_using_profil_func(self):
		found = resolve('/profil')
		self.assertEqual(found.func, views.profil)

