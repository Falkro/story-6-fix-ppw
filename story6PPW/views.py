from django.http import HttpResponse
from django.shortcuts import render
from .forms import statusForm
from .models import Status

# Create your views here.

def index(request):
    form = statusForm()

    response = {'form' : form, 'all' : Status.objects.all().reverse()}
    return render(request, 'index.html', response)

response = {}
def addstatus(request):
	if request.method == 'POST':
		form = statusForm(request.POST or None)
		if form.is_valid():
			response['status'] = request.POST['status']
			status = Status(status=response['status'])

			status.save()
			return render(request, 'addstatus.html', response)

	else:
		return render(request, 'addstatus.html')
		
def profil(request):
	return render(request, 'profil.html')

